// MaliszewskiKarol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

int main()
{
	string name, surname, address, phone;
	int number;
	bool back =  false;

	do
	{
		cout << "Podaj swoje imie" << endl;
		cin >> name;
		cout << "Podaj nazwisko" << endl;
		cin >> surname;
		cout << "Podaj adres" << endl;
		cin >> address;
		cout << "Podaj numer telefonu" << endl;
		cin >> phone;

		cout << "Chcesz poprawic swoje dane wpisz:" << endl;
		cout << "1 - tak inna liczba - wypisze podane przez Ciebie dane" << endl;
		cin >> number;
		if (number == 1)
		{
			back = true;
		}
		else if (number != 1)
		{
			break;
		}
	} while (back);

	cout << name << endl;
	cout << surname << endl;
	cout << address << endl;
	cout << phone << endl;

	cout << "Milego dnia" << endl;
	system("pause");

    return 0;
}

